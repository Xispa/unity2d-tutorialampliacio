﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Character : MonoBehaviour {

    public float Speed = 0.0f;
    public float lateralMovement = 2.0f;
    public float jumpMovement = 400.0f;

    private float movementButton = 0.0f;

    public Transform groundCheck;

    private Animator animator;
    private Rigidbody2D rigidbody2d;

    public bool grounded = true;
    
    void Start()
    {
        animator = GetComponent<Animator>();
        rigidbody2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        grounded = Physics2D.Linecast(transform.position,
                                      groundCheck.position,
                                      1 << LayerMask.NameToLayer("Ground"));
        
        /*
        if (grounded && Input.GetButtonDown("Jump"))
            rigidbody2d.AddForce(Vector2.up * jumpMovement);
        */
        
        if (grounded)
            animator.SetTrigger("Grounded");
        else
            animator.SetTrigger("Jump");

        // Speed = lateralMovement * Input.GetAxis("Horizontal");
        Speed = lateralMovement * movementButton;

        transform.Translate(Vector2.right * Speed * Time.deltaTime);

        animator.SetFloat("Speed", Mathf.Abs(Speed));

        if (Speed < 0)
            transform.localScale = new Vector3(-1, 1, 1);
        else
            transform.localScale = new Vector3(1, 1, 1);
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Star")
        {
            Star.numStars--;
            Destroy(collider.gameObject);
            print("STARS REMAINIG: " + Star.numStars);
        }
        if (Star.numStars == 0)
            print("ALL STARS COLLECTED !!!!!");
        
        if (collider.tag == "ZOOM")
            GameObject.Find("MainVirtual").GetComponent<CinemachineVirtualCamera>().enabled = false;
    }
    
    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag == "ZOOM")
            GameObject.Find("MainVirtual").GetComponent<CinemachineVirtualCamera>().enabled = true;
    }
    
    void OnCollisionEnter2D(Collision2D collider) {
        if (collider.gameObject.tag == "MobilePlatform")
            transform.SetParent (collider.transform);
    }
    void OnCollisionExit2D(Collision2D collider) {
        if (collider.gameObject.tag == "MobilePlatform")
            transform.SetParent (null);
    }

    public void Jump()
    {
        if (grounded)
                rigidbody2d.AddForce(Vector2.up * jumpMovement);
    }

    public void Move(float amount)
    {
        movementButton = amount;
    }
}
